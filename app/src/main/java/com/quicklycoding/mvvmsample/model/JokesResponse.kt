package com.quicklycoding.mvvmsample.model

import com.quicklycoding.mvvmsample.room.entities.Joke

data class JokesResponse(
    val status: Int? = null,
    val message: String? = null,
    val jokes: List<Joke>? = null
)