package com.quicklycoding.mvvmsample.model

import com.quicklycoding.mvvmsample.room.entities.User


data class AuthResponse(
    val status: Int? = null,
    val message: String? = null,
    val user: User? = null
)