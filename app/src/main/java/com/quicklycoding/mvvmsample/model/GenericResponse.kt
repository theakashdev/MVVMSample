package com.quicklycoding.mvvmsample.model


data class GenericResponse(
    val status: Int? = null,
    val message: String? = null
)