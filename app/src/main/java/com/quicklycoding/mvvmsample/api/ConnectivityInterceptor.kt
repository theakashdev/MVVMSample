package com.quicklycoding.mvvmsample.api

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class NoInternetException(message: String): IOException(message)

class ConnectivityInterceptor(private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!context.applicationContext.isInternetAvailable()){
            throw NoInternetException("No Internet Available")
        }
        return chain.proceed(chain.request())
    }

    private fun Context.isInternetAvailable(): Boolean {

        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //API > 23
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

            capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))

        } else {//API < 23

            val activeNetwork = cm.activeNetworkInfo

            activeNetwork != null && activeNetwork.isConnected
        }

    }
}
