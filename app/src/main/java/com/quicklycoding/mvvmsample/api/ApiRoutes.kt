package com.quicklycoding.mvvmsample.api

import com.quicklycoding.mvvmsample.model.AuthResponse
import com.quicklycoding.mvvmsample.model.GenericResponse
import com.quicklycoding.mvvmsample.model.JokesResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiRoutes {

    @FormUrlEncoded
    @POST("sign-up")
    suspend fun signUp(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("login")
    suspend fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<AuthResponse>


    @FormUrlEncoded
    @POST("update-profile")
    suspend fun updateProfile(
        @Field("user_id") userId: Int,
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("dob") dob: String,
        @Field("gender") gender: String
    ): Response<GenericResponse>

    @GET("jokes")
    suspend fun getJokes(): Response<JokesResponse>
}