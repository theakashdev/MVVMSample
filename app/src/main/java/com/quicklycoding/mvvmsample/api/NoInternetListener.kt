package com.quicklycoding.mvvmsample.api

interface NoInternetListener {
    fun noInternet(message: String)
}