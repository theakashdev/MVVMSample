package com.quicklycoding.mvvmsample

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import com.quicklycoding.mvvmsample.api.ConnectivityInterceptor
import com.quicklycoding.mvvmsample.api.RetrofitBuilder
import com.quicklycoding.mvvmsample.repository.AuthRepository
import com.quicklycoding.mvvmsample.repository.JokesRepository
import com.quicklycoding.mvvmsample.repository.UserRepository
import com.quicklycoding.mvvmsample.room.AppDatabase
import com.quicklycoding.mvvmsample.ui.auth.AuthViewModel
import com.quicklycoding.mvvmsample.ui.main.home.HomeViewModel
import com.quicklycoding.mvvmsample.ui.main.profile.ProfileViewModel
import com.quicklycoding.mvvmsample.util.DIViewModelFactory
import com.quicklycoding.mvvmsample.util.bindViewModel
import org.kodein.di.*
import org.kodein.di.android.x.androidXModule

class BaseApplication : Application(), DIAware {
    override val di by DI.lazy {
        import(androidXModule(this@BaseApplication))
        /* bindings */

        bind() from singleton {ConnectivityInterceptor(instance())  }
        bind() from singleton {RetrofitBuilder(instance())  }
        bind() from singleton {AppDatabase(instance())  }

        bind() from provider { AuthRepository(instance(), instance()) }
        bind() from provider { UserRepository(instance(), instance()) }
        bind() from provider { JokesRepository(instance(), instance()) }

        bind<ViewModelProvider.Factory>() with singleton { DIViewModelFactory(di.direct) }

        bindViewModel<ProfileViewModel>() with provider { ProfileViewModel(instance()) }
        bindViewModel<HomeViewModel>() with provider { HomeViewModel(instance(), instance()) }
        bindViewModel<AuthViewModel>() with provider { AuthViewModel(instance()) }

    }
}