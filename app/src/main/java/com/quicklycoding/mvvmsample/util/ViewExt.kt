package com.quicklycoding.mvvmsample.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.navigation.NavOptions
import com.google.android.material.snackbar.Snackbar
import com.quicklycoding.mvvmsample.R

fun View.snackbar(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(this, message, duration).show()
}

fun Context.toast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}

// Copy Text
fun Context.copyText(text: String) {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("", "" + text)
    clipboard.setPrimaryClip(clip)
}

// Normal Share
fun Context.shareText(message: String) {
    Intent(Intent.ACTION_SEND).apply {
        type = "text/plain"
        putExtra(Intent.EXTRA_TITLE, R.string.app_name)
        putExtra(Intent.EXTRA_TEXT, message)
        startActivity(this)
    }
}


fun getNavOptions(): NavOptions {
    return NavOptions.Builder().apply {
        setEnterAnim(R.anim.fade_in)
        setExitAnim(R.anim.fade_out)
        setPopEnterAnim(R.anim.fade_in)
        setPopExitAnim(R.anim.fade_out)
    }.build()

}