package com.quicklycoding.mvvmsample.repository

import android.util.Log
import com.quicklycoding.mvvmsample.api.ApiRoutes
import com.quicklycoding.mvvmsample.room.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserRepository(
    private val api: ApiRoutes,
    private val database: AppDatabase
) : BaseRepository() {

    fun getLoggedInUser() = database.userDao().getUser()

    fun logout(){
        CoroutineScope(Dispatchers.IO).launch {
            database.userDao().deleteUser()
        }
    }

    suspend fun updateProfile(userId:Int, name: String, email: String, dob: String, gender: String) =
        withContext(Dispatchers.IO) {
            val response = apiCall { api.updateProfile(userId, name, email, dob, gender) }
            if (response.status == 200) database.userDao().updateUser(name, email, dob, gender)
            response
        }

}