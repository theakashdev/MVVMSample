package com.quicklycoding.mvvmsample.repository

import com.quicklycoding.mvvmsample.api.ApiRoutes
import com.quicklycoding.mvvmsample.room.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AuthRepository(
    private val api: ApiRoutes,
    private val database: AppDatabase
) : BaseRepository() {


    suspend fun login(email: String, password: String) = withContext(Dispatchers.IO) {
        val response = apiCall { api.login(email, password) }
        if (response.status == 200) database.userDao().save(response.user!!)
        response
    }

    suspend fun signUp(name: String, email: String, password: String) =
        withContext(Dispatchers.IO) {
            val response = apiCall { api.signUp(name, email, password) }
            if (response.status == 200) database.userDao().save(response.user!!)
            response
        }


}