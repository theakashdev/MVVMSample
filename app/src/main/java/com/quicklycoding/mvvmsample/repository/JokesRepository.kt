package com.quicklycoding.mvvmsample.repository

import com.quicklycoding.mvvmsample.api.ApiRoutes
import com.quicklycoding.mvvmsample.room.AppDatabase
import com.quicklycoding.mvvmsample.room.entities.Joke
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class JokesRepository(
    private val api: ApiRoutes,
    private val database: AppDatabase
) : BaseRepository() {

    fun getSavedJokes() = database.jokesDao().getJokes()

    suspend fun fetchJokes() = withContext(Dispatchers.IO) {
        val response = apiCall { api.getJokes() }
        if (response.status == 200) saveJokes(response.jokes!!)
        response
    }

    private suspend fun saveJokes(jokes: List<Joke>) {
        CoroutineScope(Dispatchers.Default).launch {
            database.jokesDao().saveJokes(jokes)
        }
    }

}