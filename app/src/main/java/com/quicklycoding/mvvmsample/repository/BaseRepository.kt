package com.quicklycoding.mvvmsample.repository

import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException

class ApiException(message: String) : IOException(message)

abstract class BaseRepository {

    suspend fun <T : Any> apiCall(call: suspend () -> Response<T>): T {
        val response = call.invoke()

        when {
            response.isSuccessful -> return response.body()!!

            else -> {
                val errorResponse = response.errorBody()!!.charStream().readText()

                var message = ""
                try {
                    val jsonObj = JSONObject(errorResponse)

                    val errorMsg = jsonObj.getString("message")
                    val errorCode = response.code()

                    message = "Error code: $errorCode\nError message: $errorMsg"

                } catch (ex: JSONException) {
                }
                throw ApiException(message)
            }
        }
    }
}