package com.quicklycoding.mvvmsample.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Joke(
    @PrimaryKey(autoGenerate = false)
    val id: Int? = null,
    val joke: String? = null
)