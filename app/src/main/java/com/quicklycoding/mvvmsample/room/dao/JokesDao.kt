package com.quicklycoding.mvvmsample.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.quicklycoding.mvvmsample.room.entities.Joke

@Dao
interface JokesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveJokes(jokes: List<Joke>)

    @Query("SELECT * FROM Joke")
    fun getJokes(): LiveData<List<Joke>>

    @Query("DELETE FROM Joke")
    suspend fun deleteAllJokes()
}