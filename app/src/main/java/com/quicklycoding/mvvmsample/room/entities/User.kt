package com.quicklycoding.mvvmsample.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = false)
    val id: Int? = null,

    val name: String? = null,
    val email: String? = null,
    val dob: String? = null,
    val gender: String? = null,
    val updated_at: String? = null,
    val created_at: String? = null

)