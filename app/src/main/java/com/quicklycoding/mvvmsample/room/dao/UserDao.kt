package com.quicklycoding.mvvmsample.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.quicklycoding.mvvmsample.room.entities.User

@Dao
interface UserDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(user: User): Long

    @Query("SELECT * FROM User")
    fun getUser(): LiveData<User>

    @Query("UPDATE User SET name = :name, email = :email, dob = :dob, gender = :gender")
    suspend fun updateUser(name: String, email:String, dob:String, gender:String)


    @Query("DELETE FROM User")
    suspend fun deleteUser()
}