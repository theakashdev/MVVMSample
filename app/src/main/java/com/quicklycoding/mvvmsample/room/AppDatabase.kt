package com.quicklycoding.mvvmsample.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.quicklycoding.mvvmsample.room.dao.JokesDao
import com.quicklycoding.mvvmsample.room.dao.UserDao
import com.quicklycoding.mvvmsample.room.entities.Joke
import com.quicklycoding.mvvmsample.room.entities.User

@Database(entities = [User::class, Joke::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun jokesDao(): JokesDao

    companion object {
        private const val DB_NAME = "my_room_database"
        operator fun invoke(context: Context): AppDatabase {

            return Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                DB_NAME
            ).fallbackToDestructiveMigration()
                .build()
        }
    }

}