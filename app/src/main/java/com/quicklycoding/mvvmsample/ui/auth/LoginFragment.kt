package com.quicklycoding.mvvmsample.ui.auth

import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.databinding.FragmentLoginBinding
import com.quicklycoding.mvvmsample.ui.base.BaseFragment
import com.quicklycoding.mvvmsample.util.kodeinViewModel

class LoginFragment : BaseFragment<AuthViewModel, FragmentLoginBinding>(R.layout.fragment_login) {


    override val viewModel: AuthViewModel by kodeinViewModel()


}