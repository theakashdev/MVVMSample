package com.quicklycoding.mvvmsample.ui.main.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.api.NoInternetListener
import com.quicklycoding.mvvmsample.databinding.FragmentHomeBinding
import com.quicklycoding.mvvmsample.room.entities.Joke
import com.quicklycoding.mvvmsample.ui.base.BaseFragment
import com.quicklycoding.mvvmsample.util.*

class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>(R.layout.fragment_home),
    OnJokeClickListener, NoInternetListener {

    override val viewModel: HomeViewModel by kodeinViewModel()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        setupJokes()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.home_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_one -> {
                requireContext().toast("One menu")
                true
            }
            R.id.menu_two -> {
                requireView().snackbar("Menu Two")
                true
            }
            else -> false

        }
    }

    private fun setupJokes() {
        viewModel.savedJokes.observe(viewLifecycleOwner, Observer { jokes ->
            if (jokes.count() == 0) viewModel.fetchJokes()
            binding.recyclerView.adapter = JokesAdapter(jokes, this)
        })
    }

    override fun onJokeClick(view: View, joke: Joke) {
        when (view.id) {
            R.id.image_copy_joke -> {
                view.context.copyText(joke.joke!!)
                view.snackbar("Jokes copied successful!")
            }

            R.id.image_share_joke -> requireContext().shareText(joke.joke!!)
        }
    }


}