package com.quicklycoding.mvvmsample.ui.auth

import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.databinding.FragmentSignupBinding
import com.quicklycoding.mvvmsample.ui.base.BaseFragment
import com.quicklycoding.mvvmsample.util.kodeinViewModel

class SignUpFragment :
    BaseFragment<AuthViewModel, FragmentSignupBinding>(R.layout.fragment_signup) {

    override val viewModel: AuthViewModel by kodeinViewModel()
}