package com.quicklycoding.mvvmsample.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import com.quicklycoding.mvvmsample.api.NoInternetListener
import com.quicklycoding.mvvmsample.util.snackbar
import com.quicklycoding.mvvmsample.util.toast
import org.kodein.di.DIAware
import org.kodein.di.android.x.di

abstract class BaseFragment<VM : BaseViewModel, T : ViewDataBinding>(
    @LayoutRes private val layout: Int
) : Fragment(), DIAware, NoInternetListener {

    lateinit var binding: T
    abstract val viewModel: VM
//    abstract val bindingVariable: Int

    override val di by di()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layout, container, false)
        binding.lifecycleOwner = this
//        binding.setVariable(bindingVariable, viewModel)
        binding.setVariable(BR.vm, viewModel)
        viewModel.noInternetListener = this
        return binding.root
    }


    override fun noInternet(message: String) {
        requireContext().toast(message)
        binding.root.snackbar(message)
    }


}