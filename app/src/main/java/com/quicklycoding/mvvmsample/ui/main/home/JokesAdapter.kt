package com.quicklycoding.mvvmsample.ui.main.home

import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.databinding.ItemJokeBinding
import com.quicklycoding.mvvmsample.room.entities.Joke
import com.quicklycoding.mvvmsample.ui.base.BaseRecyclerAdapter

class JokesAdapter(private val jokes: List<Joke>, private val listener: OnJokeClickListener) :
    BaseRecyclerAdapter<ItemJokeBinding>(jokes, R.layout.item_joke) {

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val joke = jokes[position]
        binding.joke = joke

        binding.imageCopyJoke.setOnClickListener { view ->
            listener.onJokeClick(view, joke)
        }
        binding.imageShareJoke.setOnClickListener {
            listener.onJokeClick(it, joke)
        }
    }

}