package com.quicklycoding.mvvmsample.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.ui.main.MainActivity
import com.quicklycoding.mvvmsample.ui.main.profile.ProfileViewModel
import com.quicklycoding.mvvmsample.util.kodeinViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.kodein.di.DIAware
import org.kodein.di.android.x.di
import org.kodein.di.direct
import org.kodein.di.instance

class LauncherFragment : Fragment(R.layout.fragment_launcher), DIAware {

    override val di by di()

    private val viewModel: ProfileViewModel by kodeinViewModel()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.user.observe(viewLifecycleOwner, Observer { user ->
            GlobalScope.launch(Dispatchers.Main) {
                delay(2000)

                if (user != null) {
                    val context = (requireContext() as AppCompatActivity)
                    Intent(context, MainActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        context.startActivity(this)
                    }
                } else {
                    findNavController().navigate(R.id.action_launcherFragment_to_loginFragment)
                }

            }


        })


    }

}