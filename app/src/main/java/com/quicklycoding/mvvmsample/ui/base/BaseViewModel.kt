package com.quicklycoding.mvvmsample.ui.base

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quicklycoding.mvvmsample.api.NoInternetListener

abstract class BaseViewModel : ViewModel() {

    var noInternetListener: NoInternetListener? = null

    val _progressVisibility = MutableLiveData(View.GONE)
    val progressVisibility: LiveData<Int> = _progressVisibility


    fun showProgress() {
        _progressVisibility.value = View.VISIBLE
    }

    fun hideProgress() {
        _progressVisibility.value = View.GONE
    }

}