package com.quicklycoding.mvvmsample.ui.main.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.api.NoInternetException
import com.quicklycoding.mvvmsample.repository.ApiException
import com.quicklycoding.mvvmsample.repository.JokesRepository
import com.quicklycoding.mvvmsample.repository.UserRepository
import com.quicklycoding.mvvmsample.ui.base.BaseViewModel
import com.quicklycoding.mvvmsample.util.getNavOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(
    private val jokesRepository: JokesRepository,
    private val userRepository: UserRepository
) : BaseViewModel() {

    val user = userRepository.getLoggedInUser()

    private val _randomNum = MutableLiveData<String>()
    val randomNum: LiveData<String> = _randomNum

    fun onRandomNoButtonClicked() {
        _randomNum.value = "No is: ${(0..100).random()}"
    }

    val savedJokes = jokesRepository.getSavedJokes()

    fun fetchJokes() {
        CoroutineScope(Dispatchers.Main).launch {
            showProgress()
            try {
                jokesRepository.fetchJokes()
            } catch (ex: ApiException) {
            } catch (ex: NoInternetException) {
                noInternetListener?.noInternet(ex.message!!)
            }
            hideProgress()
        }
    }

    fun goToAnotherFragment(view: View) {
        view.findNavController().navigate(R.id.navigation_other, null, getNavOptions())
    }


}