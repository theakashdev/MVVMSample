package com.quicklycoding.mvvmsample.ui.main.profile

import android.view.View
import com.quicklycoding.mvvmsample.api.NoInternetException
import com.quicklycoding.mvvmsample.repository.ApiException
import com.quicklycoding.mvvmsample.repository.UserRepository
import com.quicklycoding.mvvmsample.ui.base.BaseViewModel
import com.quicklycoding.mvvmsample.util.snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    val user = userRepository.getLoggedInUser()

    fun logout() = userRepository.logout()

    fun updateProfile(
        view: View,
        userId: Int,
        name: String,
        email: String,
        dob: String,
        gender: String
    ) {

        if (name.isEmpty()) {
            view.snackbar("Please enter your name")
            return
        }

        if (email.isEmpty()) {
            view.snackbar("Please enter your email")
            return
        }

        if (dob.isEmpty()) {
            view.snackbar("Please choose your dob")
            return
        }


        if (gender.isEmpty()) {
            view.snackbar("Please choose your gender")
            return
        }


        // update api...........

        CoroutineScope(Dispatchers.Main).launch {
            showProgress()
            try {
                val response = userRepository.updateProfile(userId, name, email, dob, gender)

                view.snackbar(response.message!!)

            } catch (e: NoInternetException) {
                noInternetListener?.noInternet(e.message!!)
            } catch (e: ApiException) {
                view.snackbar(e.message!!)
            }
            hideProgress()
        }
    }
}