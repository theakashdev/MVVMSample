package com.quicklycoding.mvvmsample.ui.main.other

import androidx.fragment.app.Fragment
import com.quicklycoding.mvvmsample.R

class OtherFragment : Fragment(R.layout.fragment_other)