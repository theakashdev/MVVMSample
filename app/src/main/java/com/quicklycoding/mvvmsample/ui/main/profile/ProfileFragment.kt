package com.quicklycoding.mvvmsample.ui.main.profile

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.afollestad.date.dayOfMonth
import com.afollestad.date.month
import com.afollestad.date.year
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.datePicker
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.databinding.FragmentProfileBinding
import com.quicklycoding.mvvmsample.ui.auth.AuthActivity
import com.quicklycoding.mvvmsample.ui.base.BaseFragment
import com.quicklycoding.mvvmsample.util.kodeinViewModel
import com.quicklycoding.mvvmsample.util.toast
import org.kodein.di.DIAware
import org.kodein.di.android.x.di
import java.text.SimpleDateFormat
import java.util.*

class ProfileFragment : BaseFragment<ProfileViewModel, FragmentProfileBinding>( R.layout.fragment_profile), DIAware {

    override val viewModel: ProfileViewModel by kodeinViewModel()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        binding.editTextDob.setOnClickListener { showDatePickerDialog() }
        binding.editTextGender.setOnClickListener { showGenderDialog() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_logout -> {
                showLogoutDialog()
                true
            }
            else -> false
        }
    }

    private fun showLogoutDialog() {
        MaterialDialog(requireContext()).show {
            title(text = "Are you sure you want to logout?")

            negativeButton(text = "No") { }

            positiveButton(text = "Yes") {
                viewModel.logout()

                Intent(requireActivity(), AuthActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(this)
                }

                requireContext().toast("Logout Success!")
            }
        }
    }

    private fun showDatePickerDialog() {

        val editTextDOB = binding.editTextDob.text.toString()

        val calendar = Calendar.getInstance()
        val dateFormatter = SimpleDateFormat("dd/MM/yy", Locale.US)

        if (editTextDOB.isNotEmpty()) {
            calendar.time = dateFormatter.parse(editTextDOB)
        }

        MaterialDialog(requireContext()).show {
            datePicker(null, null, calendar) { dialog, datetime: Calendar ->

                val date = "${datetime.dayOfMonth}/${datetime.month}/${datetime.year}"

                binding.editTextDob.setText(date)
            }
        }
    }


    private fun showGenderDialog() {

        val genderList = listOf("Male", "Female")

        val editTextCurrentGender = binding.editTextGender.text.toString()

        val currentGenderIndex = genderList.indexOf(editTextCurrentGender)

        MaterialDialog(requireContext()).show {
            title(text = "Please choose your gender")

            listItemsSingleChoice(
                items = genderList,
                initialSelection = currentGenderIndex
            ) { dialog, index, text ->
                binding.editTextGender.setText(text)
            }
        }
    }


}