package com.quicklycoding.mvvmsample.ui.auth

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.quicklycoding.mvvmsample.R
import com.quicklycoding.mvvmsample.api.NoInternetException
import com.quicklycoding.mvvmsample.repository.ApiException
import com.quicklycoding.mvvmsample.repository.AuthRepository
import com.quicklycoding.mvvmsample.ui.base.BaseViewModel
import com.quicklycoding.mvvmsample.ui.main.MainActivity
import com.quicklycoding.mvvmsample.util.snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthViewModel(private val authRepository: AuthRepository) : BaseViewModel() {

    fun login(view: View, email: String, password: String) {

        if (email.isEmpty()) {
            view.snackbar("Please enter your email address")
            return
        }
        if (password.isEmpty() || password.length < 6) {
            view.snackbar("Please enter your password")
            return
        }

        // login execute..........

        CoroutineScope(Dispatchers.Main).launch {
            showProgress()
            try {
                val response = authRepository.login(email, password)

                view.snackbar(response.message!!)

                if (response.status == 200) {
                    val context = (view.context as AppCompatActivity)
                    Intent(context, MainActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        context.startActivity(this)
                    }
                }
            } catch (e: NoInternetException) {
                view.snackbar(e.message!!)
            } catch (e: ApiException) {
                view.snackbar(e.message!!)
            }
            hideProgress()
        }

    }

    fun signUp(view: View, name: String, email: String, password: String) {

        if (name.isEmpty() || name.length < 3) {
            view.snackbar("Please enter your full name")
            return
        }

        if (email.isEmpty()) {
            view.snackbar("Please enter your email address")
            return
        }
        if (password.isEmpty() || password.length < 6) {
            view.snackbar("Please enter your password")
            return
        }


        // api call..........

        CoroutineScope(Dispatchers.Main).launch {
            showProgress()
            try {
                val response = authRepository.signUp(name, email, password)

                view.snackbar(response.message!!)

                if (response.status == 200) {
                    val context = (view.context as AppCompatActivity)
                    Intent(context, MainActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        context.startActivity(this)
                    }
                }
            } catch (e: NoInternetException) {
                view.snackbar(e.message!!)
            } catch (e: ApiException) {
                view.snackbar(e.message!!)
            }
            hideProgress()
        }


    }

    fun goToLogin(view: View) {
        view.findNavController().popBackStack()
    }

    fun goToSignUp(view: View) {
        view.findNavController().navigate(R.id.action_loginFragment_to_signupFragment)
    }
}
