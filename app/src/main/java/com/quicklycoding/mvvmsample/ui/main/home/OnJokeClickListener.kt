package com.quicklycoding.mvvmsample.ui.main.home

import android.view.View
import com.quicklycoding.mvvmsample.room.entities.Joke

interface OnJokeClickListener {
    fun onJokeClick(view: View, joke: Joke)
}