package com.quicklycoding.mvvmsample.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T : ViewDataBinding>(
    private val list: List<Any>, @LayoutRes val layout: Int
) : RecyclerView.Adapter<BaseRecyclerAdapter.MyViewHolder>() {

    lateinit var binding: T

    class MyViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding =
            DataBindingUtil.inflate<T>(LayoutInflater.from(parent.context), layout, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = list.size

}