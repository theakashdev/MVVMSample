package com.quicklycoding.mvvmsample.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.quicklycoding.mvvmsample.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private val navController by lazy { findNavController(R.id.nav_host_fragment_main) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupActionBarWithNavController(navController)

        bottom_nav.setupWithNavController(navController)
        bottom_nav.setOnNavigationItemSelectedListener(this)

        handleDestinationChanged()
    }

    private fun handleDestinationChanged() {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.navigation_profile) {
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
            }
            if (destination.id == R.id.navigation_other) {
                bottom_nav.visibility = View.GONE
            } else bottom_nav.visibility = View.VISIBLE
        }
    }


    override fun onSupportNavigateUp() = navController.navigateUp()

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> startDestination(R.id.navigation_home)
            R.id.navigation_profile -> startDestination(R.id.navigation_profile)
        }

        return true
    }

    private fun startDestination(destinationId: Int) {
        if (isCurrentActiveDestination(destinationId)) {
            return
        }

        if (isDestinationExists(destinationId)) {
            navController.popBackStack(destinationId, false)
        } else {
            navController.navigate(destinationId)
        }
    }

    private fun isCurrentActiveDestination(destinationId: Int): Boolean {
        return destinationId == navController.currentDestination?.id
    }

    private fun isDestinationExists(destinationId: Int): Boolean {
        return try {
            destinationId == navController.getBackStackEntry(destinationId).destination.id
        } catch (ex: IllegalArgumentException) {
            false
        }

    }

}